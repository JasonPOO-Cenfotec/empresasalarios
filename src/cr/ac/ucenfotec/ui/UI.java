package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.Empleado;
import cr.ac.ucenfotec.bl.Jefe;
import cr.ac.ucenfotec.bl.TrabajadorComision;
import cr.ac.ucenfotec.bl.TrabajadorHora;

import java.util.ArrayList;

public class UI {

    public static void main(String[] args) {
        Jefe jefe1 = new Jefe(123,"Carlos Perez");
        TrabajadorComision trabajadorComision1 = new TrabajadorComision(456,"Ana Morales",5000,5);
        TrabajadorHora trabajadorHora1 = new TrabajadorHora(789,"Luis Fallas",40,10000);

        ArrayList<Empleado> listaEmpleados = new ArrayList<>();
        listaEmpleados.add(jefe1);
        listaEmpleados.add(trabajadorComision1);
        listaEmpleados.add(trabajadorHora1);

        for (Empleado empleadoTemp:listaEmpleados) {
            empleadoTemp.calcularSalario();
        }

        System.out.println("Jefe1: " + jefe1);
        System.out.println("Trabajador Comisión: " + trabajadorComision1);
        System.out.println("Trabajador Hora: " + trabajadorHora1);
    }

}
