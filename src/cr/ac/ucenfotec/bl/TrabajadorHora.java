package cr.ac.ucenfotec.bl;

public class TrabajadorHora extends Empleado{

    private int horasTrabajadas;
    private double precioXHora;

    public TrabajadorHora() {
    }

    public TrabajadorHora(int cedula, String nombre, int horasTrabajadas, double precioXHora) {
        super(cedula, nombre);
        this.horasTrabajadas = horasTrabajadas;
        this.precioXHora = precioXHora;
    }

    public int getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(int horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    public double getPrecioXHora() {
        return precioXHora;
    }

    public void setPrecioXHora(double precioXHora) {
        this.precioXHora = precioXHora;
    }

    public void calcularSalario(){
        double salarioTemp = 0;
        if (horasTrabajadas > 50){
            horasTrabajadas = 50;
        }
        salarioTemp = horasTrabajadas * precioXHora;
        setSalario(salarioTemp);
    }

    public String toString() {
        return super.toString() +" ,horasTrabajadas=" + horasTrabajadas +", precioXHora=" + precioXHora;
    }
}
