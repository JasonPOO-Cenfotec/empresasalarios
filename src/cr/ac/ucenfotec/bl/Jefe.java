package cr.ac.ucenfotec.bl;

public class Jefe extends Empleado{

    private final double SALARIO_FIJO= 500000;

    public Jefe() {
    }

    public Jefe(int cedula, String nombre) {
        super(cedula, nombre);
    }

    public void calcularSalario(){
        setSalario(SALARIO_FIJO);
    }

     // No es necesario en este caso sobreescribir el método equals
     // Porque hace vamos a usar el toString de la clase padre Empleado
}
