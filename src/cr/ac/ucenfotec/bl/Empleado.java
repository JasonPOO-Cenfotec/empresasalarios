package cr.ac.ucenfotec.bl;

public abstract class Empleado {

    private int cedula;
    private String nombre;
    private double salario;

    public Empleado() {
    }

    public Empleado(int cedula, String nombre) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.salario = 0;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    // Método abstracto
    public abstract void calcularSalario();

    public String toString() {
        return "cedula=" + cedula + ", nombre=" + nombre +  ", salario=" + salario;
    }
}
