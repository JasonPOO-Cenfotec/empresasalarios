package cr.ac.ucenfotec.bl;

public class TrabajadorComision extends Empleado{

    private final double SALARIO_BASE=150000;
    private double comision;
    private int cantidadPiezas;

    public TrabajadorComision() {
    }

    public TrabajadorComision(int cedula, String nombre, double comision, int cantidadPiezas) {
        super(cedula, nombre);
        this.comision = comision;
        this.cantidadPiezas = cantidadPiezas;
    }

    public double getComision() {
        return comision;
    }

    public void setComision(double comision) {
        this.comision = comision;
    }

    public int getCantidadPiezas() {
        return cantidadPiezas;
    }

    public void setCantidadPiezas(int cantidadPiezas) {
        this.cantidadPiezas = cantidadPiezas;
    }

    public void calcularSalario(){
        double salarioTemp = SALARIO_BASE + (cantidadPiezas * comision);
        setSalario(salarioTemp);
    }

    public String toString() {
        return super.toString() + ", comision=" + comision + ", cantidadPiezas=" + cantidadPiezas;
    }
}
